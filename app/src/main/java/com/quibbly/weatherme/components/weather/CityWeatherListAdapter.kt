package com.quibbly.weatherme.components.weather

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.quibbly.core.weather.CurrentCityWeather
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import android.view.LayoutInflater
import androidx.recyclerview.widget.DiffUtil
import com.bumptech.glide.load.DecodeFormat
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.quibbly.core.weather.domain.WeatherRemoteSource.Companion.LOGO_ENDPOINT
import com.quibbly.weatherme.R
import com.quibbly.weatherme.databinding.CityWeatherItemBinding
import com.quibbly.weatherme.util.GlideApp


class CityWeatherListAdapter(
    cityWeatherList: List<CurrentCityWeather> = emptyList(),
    private val cityWeatherClickListener: CityWeatherClickListener
) : RecyclerView.Adapter<CityWeatherListAdapter.CityWeatherViewHolder>() {

    private val cityWeatherList = ArrayList<CurrentCityWeather>()

    init {
        this.cityWeatherList.clear()
        this.cityWeatherList.addAll(cityWeatherList)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CityWeatherViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = DataBindingUtil.inflate<CityWeatherItemBinding>(
            layoutInflater, R.layout.city_weather_item, parent, false
        )
        return CityWeatherViewHolder(binding)
    }

    override fun getItemCount() = cityWeatherList.size

    override fun onBindViewHolder(holder: CityWeatherViewHolder, position: Int) {
        holder.bind(cityWeatherList[position])
    }

    inner class CityWeatherViewHolder(
        private val binding: CityWeatherItemBinding
    ) : RecyclerView.ViewHolder(binding.root),
        View.OnClickListener {

        init {
            binding.cardCityWeatherContainer.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            when(v?.id) {
                R.id.cardCityWeatherContainer ->
                    cityWeatherClickListener.onCityWeatherClicked(cityWeatherList[adapterPosition])
            }
        }

        fun bind(cityWeather: CurrentCityWeather) {
            binding.run {
                cityWeather.weatherList?.firstOrNull()?.run weatherList@ {
                    GlideApp.with(weatherIcon.context)
                        .load(LOGO_ENDPOINT + this@weatherList.icon + ".png")
                        .format(DecodeFormat.PREFER_RGB_565)
                        .transition(DrawableTransitionOptions.withCrossFade())
                        .into(weatherIcon)

                    weatherName.text = this@weatherList.name
                }

                tvTemperature.text = root.context.getString(
                    R.string.temperature_celcius_format,
                    cityWeather.measurements?.temperature?.toString() ?: "_"
                )

                tvCityName.text = cityWeather.name ?: ""
            }
        }

    }

    inner class AccountsListDiffCallback(
        val oldList: List<CurrentCityWeather>,
        val newList: List<CurrentCityWeather>
    ) : DiffUtil.Callback() {

        override fun getOldListSize() = oldList.size

        override fun getNewListSize() = newList.size

        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            return oldList[oldItemPosition].primaryId == newList[newItemPosition].primaryId
        }

        override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            return (oldList[oldItemPosition] == newList[newItemPosition])
        }
    }

    fun update(list: List<CurrentCityWeather>) {
        val diffResult = DiffUtil.calculateDiff(AccountsListDiffCallback(this.cityWeatherList, list))

        this.cityWeatherList.clear()
        this.cityWeatherList.addAll(list)

        diffResult.dispatchUpdatesTo(this)
    }

}

interface CityWeatherClickListener {
    fun onCityWeatherClicked(cityWeather: CurrentCityWeather)
}