package com.quibbly.weatherme.components.weather

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.quibbly.core.util.CoroutineDispatcherProvider
import com.quibbly.core.weather.domain.WeatherRepository
import javax.inject.Inject

class WeatherVMFactory @Inject constructor(
    val repository: WeatherRepository,
    val coroutineDispatcherProvider: CoroutineDispatcherProvider
) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        when {
            (modelClass.isAssignableFrom(WeatherViewModel::class.java)) -> return WeatherViewModel(
                repository,
                coroutineDispatcherProvider
            ) as T
            else -> throw IllegalArgumentException("Could not create $modelClass Unknown ViewModel Class. Creation not defined in $this")
        }
    }

}