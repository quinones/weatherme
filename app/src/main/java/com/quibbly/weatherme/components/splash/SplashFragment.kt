package com.quibbly.weatherme.components.splash

import android.content.Context
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.ViewCompat
import androidx.core.view.updatePadding
import androidx.databinding.DataBindingUtil
import com.quibbly.core.components.BaseFragment
import com.quibbly.core.util.extensions.retrieveInstanceFrom
import com.quibbly.core.util.extensions.updateWindowInsets
import com.quibbly.weatherme.R
import com.quibbly.weatherme.databinding.SplashBinding


class SplashFragment : BaseFragment() {

    private var splashNavigator: SplashNavigator? = null

    private lateinit var binding: SplashBinding

    override fun onAttach(context: Context) {
        super.onAttach(context)
        splashNavigator = retrieveInstanceFrom(context)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return DataBindingUtil.inflate<SplashBinding>(inflater, R.layout.splash, container, false).apply {
            binding = this
            lifecycleOwner = viewLifecycleOwner
        }.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.splashContainer.run {
            val topPadding = paddingTop
            val bottomPadding = paddingBottom
            ViewCompat.setOnApplyWindowInsetsListener(this) { view,insets ->
                view.updatePadding(
                    top = (topPadding + insets.systemWindowInsetTop),
                    bottom = (bottomPadding + insets.systemWindowInsetBottom)
                )
                insets.updateWindowInsets(top = 0, bottom = 0)
            }
        }

        // SetUp Display Version
        activity?.run {
            val pinfo = packageManager.getPackageInfo(packageName, 0)
            binding.tvVersion.text = getString(R.string.version_name, pinfo.versionName)
        }

        binding.tvAppName.text = getString(R.string.app_name)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        ViewCompat.requestApplyInsets(binding.splashContainer)

         /* Only Dev Build has Delay, Other builds variants return 0 splash_delay
         This delay was only added to satisfy the exam requirement. It is never
         a good practice to add a hard coded splash screen delay which do not do
         anything. */
        Handler().postDelayed({
            splashNavigator?.completeSplash()
        }, resources.getInteger(R.integer.splash_delay).toLong())
    }

    override fun onDetach() {
        splashNavigator = null
        super.onDetach()
    }

    interface SplashNavigator {
        fun completeSplash()
    }

    companion object {
        fun newInstance() = SplashFragment()
    }

}