package com.quibbly.weatherme.components.weather

import android.app.Activity
import android.app.Dialog
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.core.os.bundleOf
import androidx.fragment.app.DialogFragment

/**
 * Created by Mikacuy on 3/14/2017.
 */

class SimpleDialogFragment : DialogFragment() {

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val title = arguments?.getString(DIALOG_TITLE_KEY)
        val message = arguments?.getString(DIALOG_MESSAGE_KEY)
        val buttonName = arguments?.getString(DIALOG_POSITIVE_BUTTON_KEY)

        return AlertDialog.Builder(activity!!)
            .setTitle(title)
            .setMessage(message)
            .setPositiveButton(buttonName) { _, _ ->
                targetFragment?.onActivityResult(targetRequestCode, Activity.RESULT_OK, null)
                dismiss()
            }.create()
    }

    companion object {
        private const val DIALOG_ID = "DIALOG_ID"
        private const val DIALOG_TITLE_KEY = "DIALOG_TITLE_KEY"
        private const val DIALOG_MESSAGE_KEY = "DIALOG_MESSAGE_KEY"
        private const val DIALOG_POSITIVE_BUTTON_KEY = "DIALOG_POSITIVE_BUTTON_KEY"

        fun newInstance(
            id: Int,
            title: String,
            message: String,
            postiveButtonName: String
        ) = SimpleDialogFragment().apply {
            arguments = bundleOf(
                DIALOG_ID to id,
                DIALOG_TITLE_KEY to title,
                DIALOG_MESSAGE_KEY to message,
                DIALOG_POSITIVE_BUTTON_KEY to postiveButtonName
            )
        }
    }

}
