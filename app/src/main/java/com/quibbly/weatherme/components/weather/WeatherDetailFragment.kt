package com.quibbly.weatherme.components.weather

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.core.view.updatePadding
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.bumptech.glide.load.DecodeFormat
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.quibbly.core.components.BaseFragment
import com.quibbly.core.util.extensions.updateWindowInsets
import com.quibbly.core.weather.domain.WeatherRemoteSource.Companion.LOGO_ENDPOINT
import com.quibbly.weatherme.R
import com.quibbly.weatherme.di.Injectable
import com.quibbly.weatherme.databinding.DetailBinding
import com.quibbly.weatherme.util.GlideApp
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject


class WeatherDetailFragment : BaseFragment(), Injectable {

    @Inject
    lateinit var factory: WeatherVMFactory

    private lateinit var binding: DetailBinding

    private lateinit var vm: WeatherViewModel

    private var dateFormatter = SimpleDateFormat("hh:mm:ss a", Locale.ENGLISH)

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return DataBindingUtil.inflate<DetailBinding>(inflater, R.layout.detail, container, false).apply {
            binding = this
            lifecycleOwner = viewLifecycleOwner
        }.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        vm = ViewModelProviders.of(activity!!, factory)[WeatherViewModel::class.java]

        dateFormatter.timeZone = TimeZone.getTimeZone("GMT-4")

        if (activity != null && activity is AppCompatActivity) {
            (activity as AppCompatActivity).run {
                setSupportActionBar(binding.toolbar)
                supportActionBar?.setDisplayHomeAsUpEnabled(true)
                supportActionBar?.setDisplayShowHomeEnabled(true)
            }
        }

        binding.appbarLayout.run {
            val topPadding = paddingTop
            ViewCompat.setOnApplyWindowInsetsListener(this) { view, insets ->
                view.updatePadding(top = (topPadding + insets.systemWindowInsetTop))
                insets.updateWindowInsets(top = 0)
            }
        }

        binding.toolbar.run {
            setNavigationOnClickListener {
                activity?.onBackPressed()
            }
        }

        vm.cityWeatherUiModelLiveData.observe(viewLifecycleOwner, Observer {
            val uiState = it ?: return@Observer

            uiState.currentlySelectedCityWeather?.peek()?.run currentlySelectedWeather@{
                binding.run binding@{
                    toolbar.title = name ?: ""

                    weatherList?.firstOrNull()?.run weatherList@{
                        GlideApp.with(weatherIcon.context)
                            .load(LOGO_ENDPOINT + this@weatherList.icon + ".png")
                            .format(DecodeFormat.PREFER_RGB_565)
                            .transition(DrawableTransitionOptions.withCrossFade())
                            .into(weatherIcon)

                        weatherName.text = this@weatherList.name
                    }

                    tvTemperature.text = getString(
                        R.string.temperature_celcius_format,
                        measurements?.temperature?.toString() ?: "_"
                    )

                    tvWindValue.text = getString(
                        R.string.wind_format,
                        wind?.speed?.toString() ?: "0"
                    )

                    tvRainValue.text = getString(
                        R.string.rain_format,
                        rain?.mainPrecipitation?.toString()
                            ?: rain?.secondaryPrecipitation?.toString() ?: "_"
                    )

                    tvCloudValue.text = getString(
                        R.string.cloudiness_format,
                        clouds?.cloudiness.toString()
                    )

                    tvVisibilityValue.text = visibility.toString()

                    tvSunriseValue.text = if (sys?.sunrise != null) {
                        dateFormatter.format(Date(sys!!.sunrise!! * 1000L))
                    } else {
                        "_"
                    }

                    tvSunsetValue.text = if (sys?.sunset != null) {
                        dateFormatter.format(Date(sys!!.sunset!! * 1000L))
                    } else {
                        "_"
                    }
                }
            }
        })
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        ViewCompat.requestApplyInsets(binding.appbarLayout)
        ViewCompat.requestApplyInsets(binding.constraintDetails)
    }

    companion object {
        fun newInstance() = WeatherDetailFragment()
    }
}