package com.quibbly.weatherme.components

import android.Manifest
import android.content.DialogInterface
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import com.google.firebase.analytics.FirebaseAnalytics
import com.quibbly.core.result.Result
import com.quibbly.core.util.CoroutineDispatcherProvider
import com.quibbly.core.util.extensions.checkUngrantedPermission
import com.quibbly.core.util.extensions.requestUngrantedPermissions
import com.quibbly.core.weather.CurrentCityWeather
import com.quibbly.core.weather.domain.WeatherRemoteSource
import com.quibbly.weatherme.R
import com.quibbly.weatherme.components.splash.SplashFragment
import com.quibbly.weatherme.components.weather.WeatherDetailFragment
import com.quibbly.weatherme.components.weather.WeatherListFragment
import com.quibbly.weatherme.databinding.ActivityMainBinding
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject


class MainActivity : AppCompatActivity(),
    SplashFragment.SplashNavigator, WeatherListFragment.WeatherListNavigatior,
    HasSupportFragmentInjector {

    private lateinit var firebaseAnalytics: FirebaseAnalytics

    private lateinit var binding: ActivityMainBinding

    private val mapsPermissions: Array<String> by lazy {
        arrayOf(
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION
        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.AppTheme_Api_NoActionBar)
        super.onCreate(savedInstanceState)
        firebaseAnalytics = FirebaseAnalytics.getInstance(this)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        binding.lifecycleOwner = this
        setContentView(binding.root)

        requestUngrantedPermissions(
            mapsPermissions,
            requestCode = MAPS_PERMISSION_REQUEST_CODE
        )

        if (savedInstanceState == null) {
            showSplash()
        }
    }

    private fun showSplash() {
        supportFragmentManager.beginTransaction()
            .replace(binding.mainContainer.id, SplashFragment.newInstance())
            .commit()
    }

    override fun completeSplash() {
        supportFragmentManager.beginTransaction()
            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
            .replace(binding.mainContainer.id, WeatherListFragment.newInstance())
            .commit()
    }

    override fun showCityWeatherDetails(cityWeather: CurrentCityWeather) {
        supportFragmentManager.beginTransaction()
            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
            .addToBackStack(null)
            .replace(binding.mainContainer.id, WeatherDetailFragment.newInstance())
            .commit()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            MAPS_PERMISSION_REQUEST_CODE -> {
                val ungrantedPermissions = checkUngrantedPermission(mapsPermissions)

                if (ungrantedPermissions.isNotEmpty()) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (shouldShowRequestPermissionRationale(ungrantedPermissions[0])) {
                            AlertDialog.Builder(this@MainActivity)
                                .setMessage(getString(R.string.permissions_denied))
                                .setPositiveButton(getString(R.string.ok)) { _, i ->
                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                        val array = arrayOfNulls<String>(ungrantedPermissions.size)
                                        ungrantedPermissions.toArray(array)

                                        requestPermissions(
                                            ungrantedPermissions.toArray(
                                                arrayOfNulls<String>(ungrantedPermissions.size)
                                            ), MAPS_PERMISSION_REQUEST_CODE
                                        )
                                    }
                                }.setNegativeButton(getString(R.string.cancel), null).create().show()

                            return
                        }
                    }

                } else {
                    /*
                    * This should have been used for getting the user's current location to query the current weather for
                    * that location. Was not able to implement due to time constraints.
                    * */
                }
            }
        }
    }

    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>

    override fun supportFragmentInjector(): DispatchingAndroidInjector<Fragment> = dispatchingAndroidInjector

    companion object {
        private const val MAPS_PERMISSION_REQUEST_CODE = 66
    }
}
