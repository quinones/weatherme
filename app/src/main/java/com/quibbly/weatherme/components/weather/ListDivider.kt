package com.quibbly.weatherme.components.weather

import android.content.Context
import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.quibbly.weatherme.R

class ListDivider(val context: Context, val left: Int = 0, val top: Int = 0, val right: Int = 0, val bottom: Int = 0) : RecyclerView.ItemDecoration() {

    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
        super.getItemOffsets(outRect, view, parent, state)
        var position = parent.getChildViewHolder(view)?.adapterPosition
        if (position == RecyclerView.NO_POSITION) {
            val oldPosition = parent.getChildViewHolder(view)?.oldPosition
            if (oldPosition == RecyclerView.NO_POSITION) return
            position = oldPosition
        }

        outRect.set(
                left,
                if (position==0)
                    context.resources.getDimensionPixelOffset(R.dimen.list_top_bottom_space)
                else
                    top,
                right,
                if (position==parent.adapter?.itemCount?.minus(1))
                    context.resources.getDimensionPixelOffset(R.dimen.list_top_bottom_space)
                else
                    0
        )
    }

}