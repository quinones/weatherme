package com.quibbly.weatherme.components.weather

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import com.quibbly.core.components.BaseViewModel
import com.quibbly.core.result.Result
import com.quibbly.core.util.CoroutineDispatcherProvider
import com.quibbly.core.util.Event
import com.quibbly.core.weather.CurrentCityWeather
import com.quibbly.core.weather.domain.WeatherRepository
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class WeatherViewModel(
    private val repository: WeatherRepository,
    coroutineDispatcherProvider: CoroutineDispatcherProvider
) : BaseViewModel(coroutineDispatcherProvider) {

    private val _cityWeatherUiModelLiveData = MediatorLiveData<CityWeatherUiModel?>().apply {
        addSource(repository.cityWeatherLiveData) { list ->
            this.value = this.value?.copy(cityWeatherList = list)
        }
    }
    val cityWeatherUiModelLiveData: LiveData<CityWeatherUiModel?>
        get() = _cityWeatherUiModelLiveData

    init {
        _cityWeatherUiModelLiveData.value = CityWeatherUiModel()
    }

    private fun getCurrentCityWeatherUiModel() = _cityWeatherUiModelLiveData.value!!

    fun refreshCityWeatherList() {
        scope.launch {

            _cityWeatherUiModelLiveData.value = getCurrentCityWeatherUiModel().copy(
                isListUpdating = true
            )

            val result = withContext(coroutineDispatcherProvider.io) {
                repository.requestWeatherForCityIds(
                    SAN_FRANCISCO_ID,
                    LONDON_ID,
                    PRAGUE_ID
                )
            }

            if (result is Result.Error) {
                _cityWeatherUiModelLiveData.value = getCurrentCityWeatherUiModel().copy(
                    showErrorMessage = Event(result.exception.message),
                    isListUpdating = false
                )
            }

            _cityWeatherUiModelLiveData.value = getCurrentCityWeatherUiModel().copy(
                isListUpdating = false
            )

        }

    }

    fun setSelectedCityWeather(cityWeather: CurrentCityWeather) {
        _cityWeatherUiModelLiveData.value = getCurrentCityWeatherUiModel()
            .copy(currentlySelectedCityWeather = Event(cityWeather))
    }

    companion object {
        private val SAN_FRANCISCO_ID = 1689973
        private val LONDON_ID = 2643743
        private val PRAGUE_ID = 3067696
    }
}

/**
 * UI model
 */
data class CityWeatherUiModel(
    val isListUpdating: Boolean = false,
    val cityWeatherList: List<CurrentCityWeather> = emptyList(),
    val currentlySelectedCityWeather: Event<CurrentCityWeather?>? = null,
    val showErrorMessage: Event<String?>? = null
)