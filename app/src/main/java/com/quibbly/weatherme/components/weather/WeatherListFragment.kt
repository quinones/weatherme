package com.quibbly.weatherme.components.weather

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.core.view.updatePadding
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.quibbly.core.components.BaseFragment
import com.quibbly.core.util.extensions.retrieveInstanceFrom
import com.quibbly.core.util.extensions.updateWindowInsets
import com.quibbly.core.weather.CurrentCityWeather
import com.quibbly.weatherme.R
import com.quibbly.weatherme.databinding.ListBinding
import com.quibbly.weatherme.di.Injectable
import kotlinx.android.synthetic.main.list.*
import javax.inject.Inject

class WeatherListFragment: BaseFragment(),
    CityWeatherClickListener, Injectable {

    private var weatherListNavigator: WeatherListNavigatior? = null

    @Inject
    lateinit var factory: WeatherVMFactory

    private lateinit var binding: ListBinding

    private lateinit var vm: WeatherViewModel

    private lateinit var cityWeatherListAdapter: CityWeatherListAdapter

    override fun onAttach(context: Context) {
        super.onAttach(context)
        weatherListNavigator = retrieveInstanceFrom(context)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return DataBindingUtil.inflate<ListBinding>(inflater, R.layout.list, container, false).apply {
            binding = this
            lifecycleOwner = viewLifecycleOwner
        }.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        vm = ViewModelProviders.of(activity!!, factory)[WeatherViewModel::class.java]

        if (activity != null && activity is AppCompatActivity) {
            (activity as AppCompatActivity).setSupportActionBar(binding.toolbar)
        }

        binding.appbarLayout.run {
            val topPadding = paddingTop
            ViewCompat.setOnApplyWindowInsetsListener(this) { view, insets ->
                view.updatePadding(top = (topPadding + insets.systemWindowInsetTop))
                insets.updateWindowInsets(top = 0)
            }
        }

        cityWeatherListAdapter = CityWeatherListAdapter(cityWeatherClickListener = this)
        binding.recyclerView.run {
            val paddingBottom = paddingBottom
            ViewCompat.setOnApplyWindowInsetsListener(this) { view, insets ->
                view.updatePadding(bottom = (paddingBottom + insets.systemWindowInsetBottom))
                insets.updateWindowInsets(bottom = 0)
            }

            adapter = cityWeatherListAdapter
            layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
            addItemDecoration(
                ListDivider(context!!,
                    context?.resources?.getDimensionPixelSize(R.dimen.list_top_bottom_space) ?: 0,
                    context?.resources?.getDimensionPixelSize(R.dimen.list_top_bottom_space) ?: 0,
                    context?.resources?.getDimensionPixelSize(R.dimen.list_top_bottom_space) ?: 0,
                    context?.resources?.getDimensionPixelSize(R.dimen.list_top_bottom_space) ?: 0)
            )
        }

        binding.swipeRefresh.run {
            setColorSchemeResources(R.color.colorAccent)
            setOnRefreshListener { vm.refreshCityWeatherList() }
        }

        vm.cityWeatherUiModelLiveData.observe(viewLifecycleOwner, Observer {
            val uiState = it ?: return@Observer

            cityWeatherListAdapter.update(uiState.cityWeatherList)

            binding.swipeRefresh.isRefreshing = uiState.isListUpdating

            uiState.currentlySelectedCityWeather?.consume()?.run {
                weatherListNavigator?.showCityWeatherDetails(this)
            }

            uiState.showErrorMessage?.consume()?.run {
                val dialog = fragmentManager?.findFragmentByTag(ERROR_DIALOG_TAG)
                if (dialog != null && dialog is DialogFragment) {
                    dialog.dismiss()
                }

                SimpleDialogFragment.newInstance(
                    -1,
                    getString(R.string.error),
                    this,
                    getString(R.string.ok)
                ).show(fragmentManager!!, ERROR_DIALOG_TAG)
            }
        })
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        ViewCompat.requestApplyInsets(binding.appbarLayout)
        ViewCompat.requestApplyInsets(binding.recyclerView)
    }

    override fun onDetach() {
        weatherListNavigator = null
        super.onDetach()
    }

    override fun onCityWeatherClicked(cityWeather: CurrentCityWeather) {
        vm.setSelectedCityWeather(cityWeather = cityWeather)
    }

    companion object {
        private const val ERROR_DIALOG_TAG = "ERROR_DIALOG_TAG"

        fun newInstance() = WeatherListFragment()
    }

    interface WeatherListNavigatior {
        fun showCityWeatherDetails(cityWeather: CurrentCityWeather)
    }
}