package com.quibbly.weatherme.di

import com.quibbly.weatherme.components.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class AcitivityInjectorModule {

    @ContributesAndroidInjector(modules = [ WeatherListFragmentModule::class ])
    abstract fun contributeMainActivity(): MainActivity

}