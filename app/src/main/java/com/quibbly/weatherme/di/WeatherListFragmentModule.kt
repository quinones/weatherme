package com.quibbly.weatherme.di

import com.quibbly.weatherme.components.weather.WeatherDetailFragment
import com.quibbly.weatherme.components.weather.WeatherListFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class WeatherListFragmentModule {

    @ContributesAndroidInjector
    abstract fun contributeWeatherListFragment(): WeatherListFragment

    @ContributesAndroidInjector
    abstract fun contributeWeatherDetailFragment(): WeatherDetailFragment

}