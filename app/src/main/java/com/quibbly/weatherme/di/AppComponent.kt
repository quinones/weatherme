package com.quibbly.weatherme.di

import android.content.Context
import com.quibbly.core.di.qualifier.AppContext
import com.quibbly.core.di.scope.AppScope
import com.quibbly.core.weather.services.OpenWeatherRemoteModule
import com.quibbly.weatherme.application.WeatherMeApplication

import dagger.BindsInstance
import dagger.Component
import dagger.android.support.AndroidSupportInjectionModule

@AppScope
@Component(
    modules = [
        AndroidSupportInjectionModule::class,
        AcitivityInjectorModule::class,
        OpenWeatherRemoteModule::class
    ]
)
interface AppComponent {
    @Component.Builder
    interface Builder {
        @BindsInstance
        fun context(@AppContext appContext: Context): Builder
        fun build(): AppComponent
    }

    fun inject(application: WeatherMeApplication)
}