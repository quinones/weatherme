package com.quibbly.core.di

import com.google.gson.Gson
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.quibbly.core.BuildConfig
import com.quibbly.core.di.scope.AppScope
import com.quibbly.core.util.CoroutineDispatcherProvider
import com.quibbly.core.util.DenvelopeConverter
import dagger.Module
import dagger.Provides
import kotlinx.coroutines.Dispatchers
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.converter.gson.GsonConverterFactory


@Module
class BaseModule {

    @Provides
    @AppScope
    fun getLoggingInterceptor(): HttpLoggingInterceptor = HttpLoggingInterceptor().apply {
        level = if (BuildConfig.DEBUG) {
            HttpLoggingInterceptor.Level.BODY
        } else {
            HttpLoggingInterceptor.Level.NONE
        }
    }

    @Provides
    @AppScope
    fun getCoroutineAdapterFactory(): CoroutineCallAdapterFactory = CoroutineCallAdapterFactory()

    @Provides
    @AppScope
    fun getDevelopingFactory(gson: Gson): DenvelopeConverter = DenvelopeConverter(gson)

    @Provides
    @AppScope
    fun getGsonConveter(gson: Gson): GsonConverterFactory = GsonConverterFactory.create(gson)

    @Provides
    @AppScope
    fun getGson(): Gson = Gson()

    @Provides
    @AppScope
    fun getCoroutineDispatcherProvider(): CoroutineDispatcherProvider = CoroutineDispatcherProvider(
        Dispatchers.Main,
        Dispatchers.Default,
        Dispatchers.IO
    )

}