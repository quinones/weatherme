package com.quibbly.core.di.qualifier;

import javax.inject.Qualifier;

/**
 * Created by Archie on 7/17/2017.
 */
@Qualifier
public @interface AppContext {
}
