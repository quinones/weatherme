package com.quibbly.core.weather.domain

import androidx.lifecycle.LiveData
import com.quibbly.core.result.Result
import com.quibbly.core.weather.CurrentCityWeather

class WeatherRepository(
    private val localSource: WeatherLocalSource,
    private val remoteSource: WeatherRemoteSource
) {

    val cityWeatherLiveData: LiveData<List<CurrentCityWeather>>
        get() = localSource.retrieveStoredCityWeathers()

    suspend fun requestWeatherForCityIds(vararg ids: Int): Result<List<CurrentCityWeather>> {
        val result = remoteSource.getWeatherFor(*ids)

        if (result is Result.Success) {
            localSource.updateCityWeatherList(result.data)
        }

        return result
    }

}