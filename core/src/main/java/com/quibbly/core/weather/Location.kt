package com.quibbly.core.weather

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class Location(
    @Expose @SerializedName("lon") val latitude: Double?,
    @Expose @SerializedName("lat") val longitude: Double?
)