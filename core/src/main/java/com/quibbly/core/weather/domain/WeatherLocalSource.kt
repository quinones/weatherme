package com.quibbly.core.weather.domain

import com.quibbly.core.weather.CurrentCityWeather
import com.quibbly.core.weather.persistence.CityWeatherDao

class WeatherLocalSource(
    private val cityWeatherDao: CityWeatherDao
) {

    fun retrieveStoredCityWeathers() = cityWeatherDao.getAllStoredCityWeather()

    fun updateCityWeatherList(cityWeatherList: List<CurrentCityWeather>) {
        cityWeatherDao.insertAllCityWeatherItem(cityWeatherList)
    }

}