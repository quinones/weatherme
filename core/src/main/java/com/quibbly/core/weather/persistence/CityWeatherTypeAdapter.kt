package com.quibbly.core.weather.persistence

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.quibbly.core.weather.Weather


class CityWeatherTypeAdapter {

    companion object {
        private val gson = Gson()

        @JvmStatic
        @TypeConverter
        fun weatherListToString(list: List<Weather>?): String = gson.toJson(list)

        @JvmStatic
        @TypeConverter
        fun stringToWeatherList(data: String?): List<Weather>? {
            if (data == null) {
                return emptyList()
            }

            val listType = object : TypeToken<List<Weather>?>() {}.type

            return gson.fromJson(data, listType)
        }
    }


}

