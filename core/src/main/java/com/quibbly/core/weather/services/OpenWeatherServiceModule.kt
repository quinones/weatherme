package com.quibbly.core.weather.services

import com.quibbly.core.di.scope.AppScope
import com.quibbly.core.weather.domain.WeatherLocalSource
import com.quibbly.core.weather.domain.WeatherRemoteSource
import com.quibbly.core.weather.domain.WeatherRepository
import com.quibbly.core.weather.persistence.CityWeatherDao
import dagger.Module
import dagger.Provides

@Module
class OpenWeatherServiceModule {

    @Provides
    @AppScope
    fun provideWeatherRemoteSource(
        service: OpenWeatherService
    ) = WeatherRemoteSource(
        openWeatherService = service
    )

    @Provides
    @AppScope
    fun provideWeatherLocalSource(
        cityWeatherDao: CityWeatherDao
    ) = WeatherLocalSource(
        cityWeatherDao = cityWeatherDao
    )

    @Provides
    @AppScope
    fun provideWeatherRepository(
        localSource: WeatherLocalSource,
        remoteSource: WeatherRemoteSource
    ) = WeatherRepository(
        localSource = localSource,
        remoteSource = remoteSource
    )

}