package com.quibbly.core.weather

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class Rain(
    @Expose @SerializedName("1h") val mainPrecipitation: Double?,
    @Expose @SerializedName("3h") val secondaryPrecipitation: Double?
)