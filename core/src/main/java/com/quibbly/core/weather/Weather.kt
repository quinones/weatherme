package com.quibbly.core.weather

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class Weather(
    @Expose @SerializedName("id") val id: Int?,
    @Expose @SerializedName("main") val name: String?,
    @Expose @SerializedName("description") val description: String?,
    @Expose @SerializedName("icon") val icon: String?
)