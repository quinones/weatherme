package com.quibbly.core.weather

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class Wind(
    @Expose @SerializedName("speed") val speed: Double?,
    @Expose @SerializedName("deg") val degrees: Double?
)