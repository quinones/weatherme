package com.quibbly.core.weather

import androidx.annotation.NonNull
import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

@Entity
class CurrentCityWeather(
    @Expose @SerializedName("id")
    @NonNull @PrimaryKey
    val primaryId: String,
    @Expose @SerializedName("name")
    val name: String?,
    @Expose @SerializedName("cod")
    val cod: String?,
    @Expose @SerializedName("dt")
    val dt: Int?,
    @Expose @SerializedName("coord")
    @Embedded
    val location: Location?,
    @Expose @SerializedName("main")
    @Embedded
    val measurements: Measurements?,
    @Expose @SerializedName("visibility")
    val visibility: Double?,
    @Expose @SerializedName("wind")
    @Embedded
    val wind: Wind?,
    @Expose @SerializedName("rain")
    @Embedded
    val rain: Rain?,
    @Expose @SerializedName("clouds")
    @Embedded
    val clouds: Clouds?,
    @Expose @SerializedName("sys")
    @Embedded
    val sys: Sys?,
    @Expose @SerializedName("weather")
    val weatherList: List<Weather>? = null
)