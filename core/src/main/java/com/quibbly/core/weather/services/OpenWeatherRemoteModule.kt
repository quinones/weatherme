package com.quibbly.core.weather.services

import android.os.Build
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.quibbly.core.di.BaseModule
import com.quibbly.core.di.scope.AppScope
import com.quibbly.core.util.DenvelopeConverter
import com.quibbly.core.util.TLSSocketFactory
import com.quibbly.core.util.TrustingTrustManager
import com.quibbly.core.weather.persistence.OpenWeatherLocalModule
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.security.KeyManagementException
import java.security.NoSuchAlgorithmException
import java.util.concurrent.TimeUnit
import javax.net.ssl.*

@Module(includes = [
    BaseModule::class,
    OpenWeatherServiceModule::class,
    OpenWeatherLocalModule::class
])
class OpenWeatherRemoteModule {

    @Provides
    @AppScope
    fun provideOkHttpClient(
        logginInterceptor: HttpLoggingInterceptor,
        ssl: SSLSocketFactory?,
        trustManager: X509TrustManager,
        myHostNameVerifier: HostnameVerifier
    ): OkHttpClient = OkHttpClient.Builder()
        .addInterceptor(logginInterceptor)
        .sslSocketFactory(ssl!!, trustManager)
        .hostnameVerifier(myHostNameVerifier)
        .readTimeout(1000 * 60 * 2, TimeUnit.MILLISECONDS)
        .connectTimeout(1000 * 60 * 2, TimeUnit.MILLISECONDS)
        .build()

    @Provides
    @AppScope
    fun provideStackOverFlowRetrofit(
        okHttpClient: OkHttpClient,
        denvelopeConverter: DenvelopeConverter,
        gsonConverterFactory: GsonConverterFactory,
        coroutineAdapterFactory: CoroutineCallAdapterFactory
    ): Retrofit {
        return Retrofit.Builder()
            .baseUrl(OpenWeatherService.ENDPOINT)
            .addConverterFactory(denvelopeConverter)
            .addConverterFactory(gsonConverterFactory)
            .addCallAdapterFactory(coroutineAdapterFactory)
            .client(okHttpClient)
            .build()
    }

    @Provides
    @AppScope
    fun getStackOverflowService(retrofit: Retrofit): OpenWeatherService
            = retrofit.create(OpenWeatherService::class.java)

    @Provides
    @AppScope
    fun getCustomTrustManager(): X509TrustManager = TrustingTrustManager()

    @Provides
    @AppScope
    fun getHostnameVerifier() = HostnameVerifier { _, _ -> true }

    @Provides
    @AppScope
    fun getTrustManager(x509TrustManager: X509TrustManager): Array<TrustManager> {
        return arrayOf(x509TrustManager)
    }

    @Provides
    @AppScope
    fun getSSLContext(trustManagers: Array<TrustManager>): SSLContext? {
        var sslContext: SSLContext? = null
        try {
            sslContext = SSLContext.getInstance("TLS")
            sslContext!!.init(null, trustManagers, null)
        } catch (e: NoSuchAlgorithmException) {
            e.printStackTrace()
        } catch (e: KeyManagementException) {
            e.printStackTrace()
        }

        return sslContext
    }

    @Provides
    @AppScope
    fun getSSLSocketFactory(sslContext: SSLContext?): SSLSocketFactory? =
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.KITKAT) {
            sslContext?.let { TLSSocketFactory(it.socketFactory) }
        } else {
            sslContext?.socketFactory
        }
}