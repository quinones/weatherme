package com.quibbly.core.weather

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class Measurements(
    @Expose @SerializedName("temp") val temperature: Double?,
    @Expose @SerializedName("pressure") val pressure: Double?,
    @Expose @SerializedName("humidity") val humidity: Double?,
    @Expose @SerializedName("temp_min") val minTemperature: Double?,
    @Expose @SerializedName("temp_max") val maxTemperature: Double?
)