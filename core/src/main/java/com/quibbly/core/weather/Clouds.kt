package com.quibbly.core.weather

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class Clouds(
    @Expose @SerializedName("all") val cloudiness: Int?
)