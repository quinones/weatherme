package com.quibbly.core.weather.persistence

import android.app.Application
import android.content.Context
import android.os.Build
import androidx.room.Room
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.quibbly.core.di.BaseModule
import com.quibbly.core.di.qualifier.AppContext
import com.quibbly.core.di.scope.AppScope
import com.quibbly.core.util.DenvelopeConverter
import com.quibbly.core.util.TLSSocketFactory
import com.quibbly.core.util.TrustingTrustManager
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.security.KeyManagementException
import java.security.NoSuchAlgorithmException
import java.util.concurrent.TimeUnit
import javax.net.ssl.*

@Module
class OpenWeatherLocalModule {

    @Provides
    @AppScope
    fun provideCityWeatherDatabase(
        @AppContext applicationContext: Context
    ) = Room.databaseBuilder(
        applicationContext,
        CityWeatherDatabase::class.java,
        "City Weather"
    ).build()

    @Provides
    @AppScope
    fun provideCityWeatherDao(
        database: CityWeatherDatabase
    ) = database.cityWeatherDao()
}