package com.quibbly.core.weather.domain

import com.quibbly.core.result.Result
import com.quibbly.core.util.callWebservice
import com.quibbly.core.weather.CurrentCityWeather
import com.quibbly.core.weather.services.OpenWeatherService
import java.io.IOException

class WeatherRemoteSource(
    private val openWeatherService: OpenWeatherService
) {

    suspend fun getWeatherFor(vararg ids: Int): Result<List<CurrentCityWeather>> =
        callWebservice(
            webservice = { requestWeatherFor(*ids) },
            errorMessage = "Failed to retrieve current weather"
        )

    private suspend fun requestWeatherFor(vararg ids: Int): Result<List<CurrentCityWeather>> {
        val response = openWeatherService.getWeatherForCityIds(
            ids = ids.joinToString(separator = ","),
            unit = METRICS,
            appId = APP_ID
        ).await()

        if (response.isSuccessful) {
            val body = response.body()
            if (body != null) {
                return Result.Success(body)
            }
        }
        return Result.Error(IOException(response.message()))
    }

    companion object {
        private const val APP_ID = "aca9730e9fe291c53e3cf9c884dd774a"

        const val LOGO_ENDPOINT = "http://openweathermap.org/img/w/"
        private const val METRICS = "metric"
    }

}