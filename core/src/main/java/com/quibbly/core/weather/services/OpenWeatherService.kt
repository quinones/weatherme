package com.quibbly.core.weather.services

import com.quibbly.core.util.EnvelopePayload
import com.quibbly.core.weather.CurrentCityWeather
import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface OpenWeatherService {

    // Query Current Weather For A Given List Country Ids
    @EnvelopePayload("list")
    @GET("group")
    fun getWeatherForCityIds(
        @Query("id") ids: String,
        @Query("units") unit: String,
        @Query("appid") appId: String
    ): Deferred<Response<List<CurrentCityWeather>>>

    companion object {
        const val ENDPOINT = "http://api.openweathermap.org/data/2.5/"
    }
}