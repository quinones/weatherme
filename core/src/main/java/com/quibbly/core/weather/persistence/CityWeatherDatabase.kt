package com.quibbly.core.weather.persistence

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.quibbly.core.weather.CurrentCityWeather

@TypeConverters(CityWeatherTypeAdapter::class)
@Database(entities = [CurrentCityWeather::class], version = 1, exportSchema = false)
abstract class CityWeatherDatabase: RoomDatabase() {
    abstract fun cityWeatherDao(): CityWeatherDao
}