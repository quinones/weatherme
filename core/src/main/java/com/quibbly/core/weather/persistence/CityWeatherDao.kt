package com.quibbly.core.weather.persistence

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy.REPLACE
import androidx.room.Query
import com.quibbly.core.weather.CurrentCityWeather

@Dao
interface CityWeatherDao {

    @Query("SELECT * FROM CurrentCityWeather")
    fun getAllStoredCityWeather(): LiveData<List<CurrentCityWeather>>

    @Insert(onConflict = REPLACE)
    fun insertAllCityWeatherItem(cityWeatherList: List<CurrentCityWeather>)

}