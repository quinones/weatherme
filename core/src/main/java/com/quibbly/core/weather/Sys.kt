package com.quibbly.core.weather

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class Sys(
    @Expose @SerializedName("type") val type: Int?,
    @Expose @SerializedName("id") val sysId: Int?,
    @Expose @SerializedName("country") val country: String?,
    @Expose @SerializedName("sunrise") val sunrise: Long?,
    @Expose @SerializedName("sunset") val sunset: Long?
)