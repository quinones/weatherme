package com.quibbly.core.util

import kotlinx.coroutines.CoroutineDispatcher

class CoroutineDispatcherProvider(
        val main: CoroutineDispatcher,
        val computation: CoroutineDispatcher,
        val io: CoroutineDispatcher
)