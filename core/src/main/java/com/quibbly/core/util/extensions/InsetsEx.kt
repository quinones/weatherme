package com.quibbly.core.util.extensions

import androidx.annotation.Px
import androidx.core.view.WindowInsetsCompat

inline fun WindowInsetsCompat.updateWindowInsets(
        @Px left: Int = systemWindowInsetLeft,
        @Px top: Int = systemWindowInsetTop,
        @Px right: Int = systemWindowInsetRight,
        @Px bottom: Int = systemWindowInsetBottom
): WindowInsetsCompat = replaceSystemWindowInsets(left, top, right, bottom)