package com.quibbly.core.util

class NetworkResponseException(val code: Int): Exception()