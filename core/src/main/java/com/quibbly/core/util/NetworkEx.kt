package com.quibbly.core.util

import com.quibbly.core.result.Result
import java.io.IOException

suspend fun <T : Any> callWebservice(webservice: suspend () -> Result<T>, errorMessage: String) = try {
    webservice()
} catch (e: Exception) {
    Result.Error(IOException(errorMessage, e))
}