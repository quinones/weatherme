package com.quibbly.core.util.extensions

import android.content.Context
import androidx.fragment.app.Fragment

inline fun <reified T> Fragment.retrieveInstanceFrom(context: Context): T =
    if (context is T) {
        context
    } else {
        throw ClassCastException("$context must implement ${T::class.java}")
    }