package com.quibbly.core.util.extensions

import android.app.Activity
import android.content.pm.PackageManager
import android.os.Build

fun Activity.checkUngrantedPermission(permissions: Array<String>) =
    permissions.filter { permission ->
        !if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED
        } else {
            true
        }
    } as ArrayList

fun Activity.requestUngrantedPermissions(permissions: Array<String>, requestCode: Int) {
    val permissionList = checkUngrantedPermission(permissions)

    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
        if (permissionList.isNotEmpty()) {
            requestPermissions(permissionList.toTypedArray(), requestCode)
        }
    }
}