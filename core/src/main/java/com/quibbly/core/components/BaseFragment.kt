package com.quibbly.core.components

import androidx.fragment.app.Fragment

open class BaseFragment: Fragment()