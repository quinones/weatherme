package com.quibbly.core.components

import androidx.lifecycle.ViewModel
import com.quibbly.core.util.CoroutineDispatcherProvider
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job

open class BaseViewModel(
    protected val coroutineDispatcherProvider: CoroutineDispatcherProvider
): ViewModel() {

    protected val parentJob = Job()
    protected val scope = CoroutineScope(coroutineDispatcherProvider.main + parentJob)

    override fun onCleared() {
        super.onCleared()
        parentJob.cancel()
    }

}